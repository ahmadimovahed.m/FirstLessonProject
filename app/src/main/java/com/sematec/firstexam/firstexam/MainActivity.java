package com.sematec.firstexam.firstexam;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

public class MainActivity extends Activity {
    EditText txtFirstName;
    EditText txtLastName;
    EditText txtFatherName;
    EditText txtTel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

         txtFirstName=(EditText)findViewById(R.id.txtFirstName);
         txtLastName=(EditText)findViewById(R.id.txtLastName);
         txtFatherName=(EditText)findViewById(R.id.txtFatherName);
         txtTel=(EditText)findViewById(R.id.txtTel);
        ImageView ImgShow1=(ImageView) findViewById(R.id.ImgShow1);
        Picasso.with(this).load("http://www.iribnews.ir/files/fa/news/1396/8/4/1517024_534.jpg").into(ImgShow1);






         Button btnSubmitForm=(Button)findViewById(R.id.btnsubmitform);
        btnSubmitForm.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {


                Toast.makeText(MainActivity.this,txtFirstName.getText().toString()+" "+txtLastName.getText().toString()
                        +"*"+txtFatherName.getText().toString()+"*"+txtTel.getText().toString(),
                        Toast.LENGTH_SHORT).show();
            }


        });


        Button btnSecondPage = (Button) findViewById(R.id.btnSecondPage);
final String aa=txtFirstName.getText().toString();
        btnSecondPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainActivity.this,SecondActivity.class);
                intent.putExtra("FirstName",txtFirstName.getText().toString());
                intent.putExtra("LastName",txtLastName.getText().toString());
                intent.putExtra("FatherName",txtFatherName.getText().toString());
                intent.putExtra("Tel",txtTel.getText().toString());
              //  startActivity(intent);
                final int result = 1;
                startActivityForResult(intent, result);
            }
        });
    }
}


